package com.example.postgres;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CompanyB")
public class CompanyB extends Company {

    private String prop1;

    public CompanyB() {}

    public CompanyB(int id, String prop1) {
        super(id, "CompanyB");
        this.prop1 = prop1;
    }

    public String getProp1() {
        return prop1;
    }

    public void setProp1(String prop1) {
        this.prop1 = prop1;
    }

    @Override
    public String toString() {
        return "CompanyB{" +
                "prop1='" + prop1 + '\'' +
                '}';
    }
}