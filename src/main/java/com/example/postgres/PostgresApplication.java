package com.example.postgres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PostgresApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(PostgresApplication.class, args);

        CompanyRepository companyRepository = context.getBean(CompanyRepository.class);

        companyRepository.save(new CompanyA(1, "value1", "value2"));
        companyRepository.save(new CompanyB(2, "value1"));

        System.out.println("\n\n\n\n");
        System.out.println(companyRepository.findById(1));
        System.out.println(companyRepository.findById(2));
    }
}
