package com.example.postgres;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CompanyA")
public class CompanyA extends Company {

    private String prop1;
    private String prop2;

    public CompanyA() {}

    public CompanyA(int id, String prop1, String prop2) {
        super(id, "CompanyA");
        this.prop1 = prop1;
        this.prop2 = prop2;
    }

    @Override
    public String toString() {
        return "CompanyA{" +
                "prop1='" + prop1 + '\'' +
                ", prop2='" + prop2 + '\'' +
                '}';
    }
}
